# powershell-scripts

## To run this script:

Open Powershell as admin: type:

```Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Force```

```powershell
Invoke-WebRequest https://chocolatey.org/install.ps1 -UseBasicParsing | Invoke-Expression
```

restart powershell and run:

```powershell
Invoke-WebRequest https://bitbucket.org/agoacher/scripts/raw/f512a182a7426c3a1f68736abf261d890d4c6d84/Powershell/environment-install.ps1 | Invoke-Expression
```

Add the following to your $PROFILE
for additional functionalities
: https://bitbucket.org/agoacher/scripts/raw/f512a182a7426c3a1f68736abf261d890d4c6d84/Powershell/environment.ps1

### The following packages will be installed

This will install [Chocolatey](https://chocolatey.org/) - a windows based package manager.

It will install [git](https://git-scm.com/) and [conemu](https://conemu.github.io/) through chocolatey

### The following PowerShell modules will also be installed

[PoshGit](https://github.com/dahlbyk/posh-git) - A Powershell environment for git
Adds git information into your powershell prompt and basic tab completion for git commands.

[Get-ChildItemColor](https://github.com/joonro/Get-ChildItemColor) - Adds some color to powershells Get-ChildItem (dir, ls) command.

[PSReadLine](https://github.com/lzybkr/PSReadLine)

['z'](https://github.com/JannesMeyer/z.ps) - z will remember directories you cd into them and cache them.  It makes directory travel in powershell trivial.

Find and install ConEmu themes
https://github.com/joonro/ConEmu-Color-Themes
