<Query Kind="Program" />

void Main()
{
	var connectionString = "";
	var settings = CodeFileSettings.New()
					.WithNameSpace("NameSpace")
					.WithPath(@"c:\dev\tmp")
					.WithUsing("System")
					.WithUsing("System.Colections.Generic")
					.Build();
					
	var modelBuilder = new ModelBuilder(connectionString, settings);
	modelBuilder.Build();
					
	
}

// Define other methods and classes here

public class CodeFileSettings
{
	public class CodeFileSettingsBuilder
	{
		private string namespaceName;
		private string filePath;
		private List<string> usings;
		
		public CodeFileSettingsBuilder()
		{
			usings = new List<string>();
			namespaceName= "";
			filePath = "";
		}
		
		public CodeFileSettingsBuilder WithNameSpace(string nameSpace)
		{
			namespaceName = nameSpace;
			return this;
		}
		
		public CodeFileSettingsBuilder WithPath(string path)
		{
			if(!Directory.Exists(path))
			{
				throw new InvalidOperationException("Cannot set an invalid path");
			}
			filePath = path;
			return this;
		}
		
		public CodeFileSettingsBuilder WithUsing(string usingStatement)
		{
			usings.Add(usingStatement);
			return this;
		}
		
		public CodeFileSettings Build()
		{
			if(string.IsNullOrEmpty(namespaceName))
			{
				throw new InvalidOperationException("Needs a namespace");
			}
			
			if(string.IsNullOrEmpty(filePath))
			{
				throw new InvalidOperationException("Needs a path");
			}
			
			return new CodeFileSettings(namespaceName, filePath, usings.Distinct());
		}
	}
	
	public static CodeFileSettingsBuilder New()
	{
		return new CodeFileSettingsBuilder();
	}
	
	private CodeFileSettings(string namespaceName, string path, IEnumerable<string> usings)
	{
		NameSpace = namespaceName;
		FilePath = path;
		Usings = usings;
	}
	
	public string NameSpace {get;}

	public string FilePath { get;}
	
	public IEnumerable<string> Usings {get;}
}

public class ModelBuilder
{
	private string connectionString;
	private CodeFileSettings settings;
	public ModelBuilder(string connectionString, CodeFileSettings settings)
	{
		this.connectionString = connectionString;
		this.settings = settings;
	}

	private static IEnumerable<TableDescription> ToTables(IEnumerable<TableColumnDescription> tables)
	{
		var groups = tables.GroupBy(x => x.TableName);
		var desc = new List<TableDescription>();
		foreach (var group in groups)
		{
			var schema = group.First().Schema;
			if (string.IsNullOrEmpty(schema)) { schema = "dbo"; }

			var table = new TableDescription(group.Key, schema);

			foreach (var col in group)
			{
				table.AddColumn(new ColumnDescription(col.ColumnName, col.DataType, col.IsNullable, col.IsPrimary, col.ColumnOrder));
			}

			table.SetPrimary();
			desc.Add(table);
		}

		return desc;
	}

	private static IEnumerable<TableColumnDescription> GetColumnsFromDB(string connectionString)
	{
		var columns = new List<TableColumnDescription>();
		using (var sqlConn = new SqlConnection(connectionString))
		{
			sqlConn.Open();
			var command = new SqlCommand(@"
	 		select t.name as tablename, c.name as columnname, c.is_nullable, ty.name as data_type, i.is_primary_key, s.name as schema_name, ic.key_ordinal as column_order
 			from sys.columns c
 			inner join sys.tables t on c.object_id = t.object_id
 			inner join sys.types ty on c.system_type_id = ty.system_type_id
 			inner join sys.schemas s on t.schema_id = s.schema_id
 			left join sys.index_columns ic on ic.object_id = c.object_id and ic.column_id = c.column_id
 			left join sys.indexes i on ic.object_id = i.object_id and ic.index_id = i.index_id
			where ty.Name <> 'sysname'"
					, sqlConn);

			using (var reader = command.ExecuteReader())
			{
				while (reader.Read())
				{
					var tableName = reader.SafeGetString(0);
					var columnName = reader.SafeGetString(1);
					var isNullable = reader.SafeGetBool(2);
					var dataType = reader.SafeGetString(3);
					var isPrimaryKey = reader.SafeGetBool(4);
					var schemaName = reader.SafeGetString(5);
					var columnOrder = (int?)reader.SafeGetByte(6);

					columns.Add(new TableColumnDescription(tableName,
							columnName,
							isNullable,
							dataType,
							isPrimaryKey,
							schemaName,
							columnOrder == null ? 0 : columnOrder.Value));

				}
			}
		}

		return columns;
	}

	public bool Build()
	{
		var columns = GetColumnsFromDB(this.connectionString);
		var tables = ToTables(columns);
		
		var sb = new StringBuilder();
		
		foreach (var table in tables)
		{
			sb.Clear();
			table.ToClass(sb, settings, 0);
			
			var classFile = Path.Combine(settings.FilePath, string.Format($"{table.Name}.cs"));
			File.WriteAllText(classFile, sb.ToString());
		}
		
		return true;
	}
}

public static string IndentString(int indent)
{
	if (indent == 0) { return ""; }
	var indentArray = Enumerable.Repeat("\t", indent);

	return string.Join("", indentArray);
}


public static string GetCSTypeFromSQLType(string sqlType)
{
	switch (sqlType)
	{
		case "int":
			return "int";
		case "nvarchar":
		case "varchar":
		case "text":
		case "char":
		case "sysname":
			return "string";
		case "bit":
			return "bool";
		case "date":
		case "datetime":
		case "datetime2":
			return "DateTime";
		case "datetimeoff":
		case "datetimeoffset":
			return "DateTimeOffset";
		case "bigint":
			return "long";
		case "float":
			return "double";
		case "real":
			return "float";
		case "uniqueidentifier":
			return "Guid";
		case "tinyint":
		case "smallint":
			return "short";
		case "money":
			return "decimal";
		case "xml":
			return "string";
		case "varbinary":
			return "byte[]";
		default:
			{
				sqlType.Dump();
				throw new InvalidOperationException(sqlType);
			}
	}
}


public class TableColumnDescription
{
	public string TableName { get; private set; }

	public string ColumnName { get; private set; }

	public bool IsNullable { get; private set; }

	public string DataType { get; private set; }

	public bool IsPrimary { get; private set; }

	public string Schema { get; private set; }

	public int ColumnOrder { get; private set; }

	public TableColumnDescription(string table, string column, bool isnull, string dataType, bool isPrimary, string schema, int ordinal)
	{
		TableName = table;
		ColumnName = column;
		IsNullable = isnull;
		DataType = dataType;
		IsPrimary = isPrimary;
		Schema = schema;
		ColumnOrder = ordinal;
	}
}

public static class DataReaderExtensions
{
	public static string SafeGetString(this SqlDataReader reader, int index)
	{
		if (reader.IsDBNull(index))
		{
			return null;
		}
		return reader.GetString(index);
	}

	public static byte? SafeGetByte(this SqlDataReader reader, int index)
	{
		if (reader.IsDBNull(index))
		{
			return null;
		}
		return reader.GetByte(index);
	}

	public static bool SafeGetBool(this SqlDataReader reader, int index)
	{
		if (reader.IsDBNull(index))
		{
			return false;
		}

		return reader.GetBoolean(index);
	}
}

public class ColumnDescription
{
	public string Name { get; private set; }

	public string DataType { get; private set; }

	public bool IsNullable { get; private set; }

	public bool IsKey { get; set; }

	public int Ordinal { get; private set; }

	public ColumnDescription(string name, string type, bool isnullable, bool iskey, int ordinal)
	{
		Name = name;
		DataType = type;
		IsNullable = isnullable;
		IsKey = iskey;
		Ordinal = ordinal;
	}

	public void ToProperty(StringBuilder sb, int indent)
	{
		var indentString = UserQuery.IndentString(indent);
		if (IsKey)
		{
			sb.AppendFormat("{0}[Key, Column(Order = {1})]\r\n", indentString, Ordinal);
		}

		var nullableOperator = IsNullable ? "?" : "";

		sb.AppendFormat("{0}public {1}{2} {3} {{get;set;}}\r\n", indentString, GetCSTypeFromSQLType(DataType), nullableOperator, Name);
	}
}

public class TableDescription
{
	public string Name { get; private set; }

	public string Schema { get; private set; }

	public IReadOnlyList<ColumnDescription> Columns { get { return cols; } }

	private List<ColumnDescription> cols;

	public TableDescription(string name, string schema)
	{
		Name = name;
		Schema = schema;

		cols = new List<ColumnDescription>();
	}

	public void ToClass(StringBuilder sb, CodeFileSettings settings, int indent)
	{
		var baseIndentString = UserQuery.IndentString(indent);

		foreach (var usingStatement in settings.Usings)
		{
			sb.AppendFormat("{0}using {1};\r\n", baseIndentString, usingStatement);
		}
		sb.AppendLine();
		sb.AppendFormat("{0}namespace {1}\r\n", baseIndentString, settings.Namespace);
		sb.AppendFormat("{0}{{\r\n", baseIndentString);

		var classIndentString = UserQuery.IndentString(indent + 1);

		sb.AppendFormat("{0}[Table(\"{1}\", Schema = \"{2}\")]\r\n", classIndentString, Name, Schema);

		var className = Name.StartsWith("_") ? Name.Substring(1) : Name;
		className = className.Replace(".", "_");
		sb.AppendFormat("{0}public class {1}\r\n", classIndentString, className);

		var inheriters = new List<string>() { "ITable" };
		if (cols.Any(c => c.Name.Equals("IsArchived"))) { inheriters.Add("IAccessEnabledItem"); }

		sb.AppendFormat("{0} : {1}", UserQuery.IndentString(indent + 2), string.Join(", ", inheriters.ToArray()));

		sb.AppendFormat("{0}{{\r\n", classIndentString);

		var propertyIndent = indent + 2;

		foreach (var column in cols)
		{
			column.ToProperty(sb, propertyIndent);
		}

		sb.AppendFormat("{0}}}\r\n", classIndentString);

		sb.AppendFormat("{0}}}\r\n", baseIndentString);
	}

	public void SetPrimary()
	{
		var primaryCols = cols.Where(x => x.IsKey);
		if (primaryCols.Any() == false)
		{
			var IdCol = cols.Where(x => x.Name.EndsWith("id", StringComparison.OrdinalIgnoreCase));
			foreach (var col in IdCol)
			{
				if (Name.Contains(col.Name.Substring(0, col.Name.Length - 2)))
				{
					col.IsKey = true;
					break;
				}
			}
		}
	}

	public void AddColumn(ColumnDescription col)
	{
		cols.Add(col);
	}
}
