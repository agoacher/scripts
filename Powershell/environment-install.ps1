# Install Chocolatey packages
Write-Output "Installing Chocolatey Package: git"
choco install git.install -y
Write-Output "Installing Chocolatey Package: conemu"
choco install conemu -y

# Install PowerShell modules
Install-PackageProvider NuGet -MinimumVersion '2.8.5.201' -Force
Set-PSRepository -Name PSGallery -InstallationPolicy Trusted

# Install some handy modules
Write-Output "Installing Module: PoshGit"
Install-Module -Name 'posh-git'
Write-Output "Installing Module: Get-ChildItemColor"
Install-Module -Name 'get-childitemcolor'
Write-Output "Installing Module: PSReadLine"
Install-Module -Name 'psreadline'
Write-Output "Installing Module: Z"
Install-Module -Name 'z' -AllowClobber